<html>
<head>
    <title>Shortest Path</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/logo-nav.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="jquery/jquery-ui.js" type="text/javascript"></script>
    <script src="bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="jquery/jquery-1.12.4.js" type="text/javascript"></script>

    <script>

        const queue = [];
        const foundPath = [];
        const colorAtOnce = [5, 450, 450, 200];
        var alg = 0;
        var isSent = false;

        function Circle(x, y, r, fill, stroke) {
            this.startingAngle = 0;
            this.endAngle = 2 * Math.PI;
            this.x = x;
            this.y = y;
            this.r = r;
            this.fill = fill;
            this.stroke = stroke;

            this.draw = function () {
                ctx.beginPath();
                ctx.arc(this.x, this.y, this.r, this.startingAngle, this.endAngle);
                ctx.fillStyle = this.fill;
                //ctx.lineWidth = 3;
                ctx.fill();
                //ctx.strokeStyle = this.stroke;
                //ctx.stroke();
            }
        }

        var canvas, ctx, flag = false, dot_flag = false;
        var prevX = 0, currX = 0, prevY = 0, currY = 0;
        const minBrushRadius = 16;
        const maxBrushRadius = 124;
        var x = "rgb(30, 144, 255)", y = minBrushRadius;



        //make some circles
        var c1 = new Circle(50, 50, 20, "red", "black");
        var c2 = new Circle(200, 50, 20, "green", "black");
        //initialise our circles
        var circles = [c1, c2];

        var mousePosition;
        //track state of mousedown and up
        var isMouseDown;

        const visitedColor = {
            r: 255,
            g: 185,
            b: 60
        };

        const pathColor = 'rgb(0, 0, 0, 1)';

        const coloringInterval = setInterval(()=>{
            if (queue.length == 0 && foundPath.length > 0) {
                colorPath(foundPath);
                clearInterval(coloringInterval);
                return;
            }

            if (queue.length == 0) return;


            const count = Math.min(queue.length, colorAtOnce[alg]);
            const toColor = queue.splice(0, count);

            for (var i = 0; i < count; i++ ) {
                const vertex = toColor[i];
                const colorData = ctx.getImageData(vertex.x, vertex.y, 1, 1).data;
                const currentColor = {
                    r: colorData[0],
                    g: colorData[1],
                    b: colorData[2],
                    a: colorData[3]
                };
                colorPixel(vertex, mixColors(currentColor, visitedColor));
            }

        }, 30);


        var socket;
        $( function() {

            canvas = document.getElementById('canvas');
            ctx = canvas.getContext("2d");
            ctx.imageSmoothingEnabled = false;
            ctx.mozImageSmoothingEnabled = false;

            w = canvas.width;
            h = canvas.height;

            canvas.addEventListener("mousemove", function (e) {
                onmove(e);
            }, false);
            canvas.addEventListener("mousedown", function (e) {
                ondown(e);
            }, false);
            canvas.addEventListener("mouseup", function (e) {
                onup(e);
            }, false);
            canvas.addEventListener("mouseout", function (e) {
                onup(e);
            }, false);

            draw();
        } );

        function avgInt(first, second) {
            return parseInt((first + second)/2.0);
        }

        function mixColors(first, second) {
            const isBackground = color => color.r == 0 && color.g == 0 && color.b == 0 && color.a != 255;
            if (isBackground(first)) { first = second; }
            else if (isBackground(second)) { second = first; }

            const color = {
                r: avgInt(first.r, second.r),
                g: avgInt(first.g, second.g),
                b: avgInt(first.b, second.b),
                a: 1.0
            };
            return `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a})`;
        }

        function getMousePosition(e) {
            var rect = canvas.getBoundingClientRect();
            return {
                x: Math.round(e.x - rect.left),
                y: Math.round(e.y - rect.top)
            }
        }

        function ondown(e) {
            if (isSent) return;
            isMouseDown = true;
            mousePosition = getMousePosition(e);
            prevX = currX;
            prevY = currY;
            currX = e.clientX - canvas.offsetLeft;
            currY = e.clientY - canvas.offsetTop;
            for ( var i = 0; i < circles.length && !intersects(circles[i]); i++ );
            if (i >= circles.length) return;
            focused.state = true;
            focused.key = i;
        }

        function onmove(e) {
            if (isSent) return;
            if (!isMouseDown) return;
            mousePosition = getMousePosition(e);
            //if any circle focused
            if (focused.state) {
                //move circle
                const tmpX = circles[focused.key].x;
                const tmpY = circles[focused.key].y;
                circles[focused.key].x = mousePosition.x;
                circles[focused.key].y = mousePosition.y;
                for (var i = 0; i < circles.length; i++ ) {
                    if (i === focused.key) continue;
                    if (intersects(circles[i], circles[focused.key])) {
                        circles[focused.key].x = tmpX;
                        circles[focused.key].y = tmpY;
                        return;
                    }
                }
                draw();
                return;
            }

            const tmpX = prevX;
            const tmpY = prevY;
            prevX = currX;
            prevY = currY;
            currX = e.clientX - canvas.offsetLeft;
            currY = e.clientY - canvas.offsetTop;
            for (var i = 0; i < circles.length; i++) {
                if (intersects(circles[i])) {
                    prevX = tmpX;
                    prevY = tmpY;
                    return;
                }
            }
            draw_line();

        }

        function onup(e) {
            isMouseDown = false;
            focused.state = false;
        }

        function draw_line() {
            ctx.beginPath();
            ctx.moveTo(prevX, prevY);
            ctx.lineTo(currX, currY);
            ctx.lineJoin = 'round';
            ctx.lineCap = 'round';
            ctx.strokeStyle = x;
            ctx.lineWidth = y;
            ctx.stroke();
            ctx.closePath();
        }

        function draw() {
            //clear canvas
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            drawCircles();
        }

        //draw circles
        function drawCircles() {
            for (var i = circles.length - 1; i >= 0; i--) {
                circles[i].draw();
            }
        }

        //detects whether the mouse cursor is between x and y relative to the radius specified
        function intersects(circle1, circle2) {
            if (!circle2) {
                const circle = circle1;
                // subtract the x, y coordinates from the mouse position to get coordinates
                // for the hotspot location and check against the area of the radius
                var areaX = mousePosition.x - circle.x;
                var areaY = mousePosition.y - circle.y;
                //return true if x^2 + y^2 <= radius squared.
                return areaX * areaX + areaY * areaY <= circle.r * circle.r;
            }

            return Math.hypot(circle1.x - circle2.x, circle1.y - circle2.y) <= circle1.r + circle2.r;
        }

        var focused = {
            key: 0,
            state: false
        }

        Array.prototype.move = function (old_index, new_index) {

            if (new_index >= this.length) {
                var k = new_index - this.length;
                while ((k--) + 1) {
                    this.push(undefined);
                }
            }
            this.splice(new_index, 0, this.splice(old_index, 1)[0]);
        };

        function increase() {
            y = Math.min(y+4, maxBrushRadius);
            document.getElementById('brush_size').innerHTML='Brush size: '+ y ;
        }

        function decrease() {
            y = Math.max(y-4, minBrushRadius);
            document.getElementById('brush_size').innerHTML='Brush size: '+ y ;
        }

        function color(obj) {
            x = $(obj).css('background-color');
            console.log(x);
        }
        function erase() {
            var m = confirm("Want to clear ?");
            if (m) {
                draw();
            }
        }

        used = [];

        function colorPixel(pixel, color) {
            ctx.fillStyle = color;
            ctx.fillRect(pixel.x, pixel.y, 1, 1);
        }

        function colorVisitedVertices(vertices) {
            for (var i = 0; i < vertices.length; i++) {
                const vertex = vertices[i];
                const colorData = ctx.getImageData(vertex.x, vertex.y, 1, 1).data;
                const currentColor = {
                    r: colorData[0],
                    g: colorData[1],
                    b: colorData[2],
                    a: colorData[3]
                };
                colorPixel(vertex, mixColors(currentColor, visitedColor));
            }
        }

        function colorPath(path) {
            for (var i = 0; i < path.length; i++ ) {
                const vertex = path[i];
                colorPixel(vertex, pathColor);
            }
        }

        function disableSendButton() {
            $('#send').prop('disabled', true);
            $('#clr').prop('disabled', true);

        }

        var alg;
        function send() {
            alg = $( "#algo" ).val();

            let image = canvas.toDataURL();
            const keyword = 'base64,';
            let baseStart = image.indexOf(keyword);
            let base64 = image.substring(baseStart + keyword.length);

            socket = new WebSocket("ws://" + location.hostname + ":" + location.port + "/shortestPathSocket");
            socket.onopen = event => {
                socket.send(JSON.stringify({
                    base64,
                    startX: circles[0].x,
                    startY: circles[0].y,
                    finishX: circles[1].x,
                    finishY: circles[1].y,
                    algoType: alg
                }));
                disableSendButton();
                isSent = true;
            };

            socket.onmessage = event => {
                const data = event.data;
                const msg = JSON.parse(data);
                if (msg.type === 1) {
                    queue.push.apply(queue, msg.vertices);
                } else if (msg.type === 2) {
                    const vertices = Array.from(msg.vertices);
                    foundPath.push.apply(foundPath, vertices);
                    console.log("Found path: ");
                    console.log(vertices);
                }
            };
        }
    </script>

    <style>

        #mom{
            width: 95%;
            position: absolute;
            top: 20%;
        }

        .baby{
            text-align: center;
            float: left;
            position: relative;
            width: 7%;
            margin-left: 2% ;
        }

        .baby_label{
            color: gainsboro;
            text-align: center;
            float: left;
            position: relative;
            width: 14%;
            margin-left: 8% ;
        }

        .label{
            text-align: center;
            color: red;
            font-style: italic;
            font-family: cursive;
            width: 168px;
            border: aqua;
            border-width: thick;
            border-style: double;
            border-radius: 12px
        }

    </style>
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">
                <p style="color: darkseagreen; font-weight: bold; font-family: cursive;">Shortest path application</p>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <div id="mom">

                <div style="font-style: italic; font-weight: bold;" class="baby_label">Choose Color</div>
                {{#ids}}
                <div style="width:15px;height:15px;background:{{.}}; top: 5px;"  class="baby"  id="color{{@index}}" data="{{.}}" onclick="color(this)"></div>
                {{/ids}}

                <div style="font-style: italic; font-weight: bold;" class="baby_label">Choose Algorithm</div>
                <div class="baby">
                    <select name="speed" id="algo">
                        <option value="1">BFS</option>
                        <option value="2">Dijkstra</option>
                        <option value="3" selected>A*</option>
                    </select>
                </div>

                </div>
            </div>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container-fluid">

    <div style="padding-top: 70px;padding-bottom: 70px;">
        <canvas id="canvas" width="640" height="390" style="position:absolute;top:25%;left:20%;border:7px solid;"></canvas>
    </div>

    <div id="note" style="background-color: honeydew;width:260px; height:398px; position:absolute;top:25%;left:77%;border:2px solid; padding: 20px;">
        <h2 style="margin-bottom: 15px; font-family: cursive">Info</h2>
        <div style="width: 95%; position: absolute; margin-top: 10px;">
        {{#colors}}
          <div style="float: left; position: relative; margin-left: 2% ; width:15px;height:15px;background: {{color}}; top: 5px;"  id="color" data="rgb(30, 144, 255)"></div>
            <p style="float: left; position: relative; margin-left: 2% ; color: {{color}}"> - {{description}} </p>
            <br><br>
        {{/colors}}
            <div style="border-radius: 50%;  float: left; position: relative; margin-left: 2% ; width:15px;height:15px;background: red; top: 5px;"  id="color" data="rgb(30, 144, 255)"></div>
            <p style="float: left; position: relative; margin-left: 2% ; color: red"> -  Start point </p>
            <br><br>

            <div style="border-radius: 50%; float: left; position: relative; margin-left: 2% ; width:15px;height:15px;background: green; top: 5px;"  id="color" data="rgb(30, 144, 255)"></div>
            <p style="float: left; position: relative; margin-left: 2% ; color: green"> -  End point </p>
            <br><br>
        </div>

    </div>

    <ul class="navbar-nav ml-auto">
        <li class="nav-item" style="margin-bottom: 30px;">
            <label id="brush_size"  class="label">Brush size: 16</label>
        </li>

        <li class="nav-item" style="margin-bottom: 22px;">
            <input type="button" class="btn btn-dark" value="Increase" id="inc"  onclick="increase()" style="width: 170px;">
        </li>

        <li class="nav-item" style="margin-bottom: 22px;">
            <input type="button" class="btn btn-dark" value="Decrease" id="dcr" onclick="decrease()" style="width: 170px;">
        </li>

        <li class="nav-item" style="margin-bottom: 22px;">
            <input type="button" class="btn btn-info" value="Clear" id="clr"  onclick="erase()" style="width: 170px;">
        </li>

        <li class="nav-item">
            <input type="button" class="btn btn-success" value="Send" id="send" onclick="send()" style="width: 170px;">
        </li>
    </ul>

</div>
<!-- /.container -->

<!-- Bootstrap core JavaScript -->

</body>
</html>
