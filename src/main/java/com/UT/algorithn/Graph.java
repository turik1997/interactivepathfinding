package com.UT.algorithn;

import java.util.List;

public abstract class Graph {

    public abstract List<Node> getNeighbors(Node node);
    public abstract int size();
    public abstract Node getNode(int id);
    public abstract double dist(Node u, Node v);
}
