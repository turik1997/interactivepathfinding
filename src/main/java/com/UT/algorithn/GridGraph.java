package com.UT.algorithn;

import com.UT.server.Server;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;

public class GridGraph extends Graph {

    private int width;
    private int height;
    private BufferedImage img;
    private final Node[] vertices;
    private int[] x_dir = {1, 1, 0, -1, -1, -1, 0, 1};
    private int[] y_dir = {0, 1, 1, 1, 0, -1, -1, -1};
    private final Node dummyNode = new Node(-1, new Color(0, 0, 0));

    public GridGraph(String base64) {
        byte[] buffer = Base64.getDecoder().decode(base64);
        BufferedImage img = null;
        try {
            img = ImageIO.read(new ByteArrayInputStream(buffer));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.width = ServerParams.CANVAS_W;
        this.height = ServerParams.CANVAS_H;
        this.vertices = new Node[this.width * this.height];
        this.img = img;
    }

    public Node getNode(int x, int y) {
        final int id = getId(x, y);
        if (vertices[id] != null) return vertices[id];
        int bitColor = this.img.getRGB(x, y);
        Color color = new Color(bitColor);
        return vertices[id] = new Node(id, color);
    }

    private int getId(int x, int y) {
        return y * this.width + x;
    }

    @Override
    public List<Node> getNeighbors(Node node) {
        List<Node> neighbors = new LinkedList<>();
        int y = getY(node);
        int x = getX(node);
        for ( int i = 0; i < x_dir.length; i++ ) {
            int nx = x + x_dir[i]; //new x
            int ny = y + y_dir[i]; //new y
            if (nx >= this.width || ny >= this.height || ny < 0 || nx < 0) continue;
            neighbors.add(getNode(nx, ny));
        }
        return neighbors;
    }

    @Override
    public int size() {
        return vertices.length;
    }

    @Override
    public Node getNode(int id) {
        if (id == -1) return dummyNode;
        if (id >= vertices.length) return null;

        return vertices[id];
    }

    private int getX(Node node) {
        return node.getId() % this.width;
    }

    private int getY(Node node) {
        return node.getId() / this.width;
    }

    @Override
    public double dist(Node u, Node v) {
        return Math.hypot(getX(u) - getX(v), getY(u) - getY(v));
    }

}
