package com.UT.algorithn;

import java.util.List;

public interface IAlgorithm {

    List<Node> solve();
}
