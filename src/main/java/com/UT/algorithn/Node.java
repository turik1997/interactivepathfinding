package com.UT.algorithn;

import java.awt.*;

public class Node {
    private final transient int id;
    public final transient int r;
    public final transient int g;
    public final transient int b;
    public final transient int a;
    public final int x;
    public final int y;

    public Node(int id, Color color) {
        this.id = id;
        this.r = color.getRed();
        this.g = color.getGreen();
        this.b = color.getBlue();
        this.a = color.getAlpha();
        this.x = this.id % ServerParams.CANVAS_W;
        this.y = this.id / ServerParams.CANVAS_W;
    }

    public int getId() {
        return id;
    }
}
