package com.UT.algorithn;

import com.UT.server.Updater;

import java.awt.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class Dijkstra extends Algorithm {

    private final PriorityQueue<PrioritizedNode> pq;
    private final double[] distance;

    public Dijkstra(Graph graph, Node start, Node destination, Updater updater) {
        super(graph, start, destination, updater::update);
        this.pq = new PriorityQueue<>(Comparator.comparingDouble(u -> u.getPriority()));
        this.distance = new double[graph.size()];
        Arrays.fill(this.distance, Integer.MAX_VALUE);
        this.distance[start.getId()] = 0;
    }

    @Override
    public List<Node> solve() {
        Node current = start;
        pq.add(new PrioritizedNode(this.distance[current.getId()], graph.getNode(-1), current));
        while (!pq.isEmpty()) {
            current = nextNode();
            visited(current);
            markUsed(current);
            if (hasReached(current)) break;
            calculateNeighbors(current);
        }
        List<Node> path = restorePath(this.start, current);
        return path;
    }

    protected double dist(Node u, Node v) {
        return graph.dist(u, v);
    }

    protected double heuristics(Node u, Node v) {
        return 0.0;
    }

    private Node nextNode() {
        PrioritizedNode pn = null;
        while (!pq.isEmpty()) {
            pn = pq.poll();
            if (!isUsed(pn.getNode())) break;
        }
        Node node = pn.getNode();
        setParent(pn.getParent(), node);
        return node;
    }

    private void calculateNeighbors(Node node) {
        List<Node> adj = this.graph.getNeighbors(node);
        for (Node n : adj) {
            if (isUsed(n)) continue;
            Color color = new Color(n.r, n.g, n.b);
            Integer id = ServerParams.valueToName.get(color.getRGB());
            if (id != null && id == ServerParams.ColorName.RED) {
                System.out.println("in red");
            }
            if (!isAllowed(color)) continue;
            double newDist = distance[node.getId()] + dist(node, n) * cost(color);
            if (newDist < distance[n.getId()]) {
                distance[n.getId()] = newDist;
                this.pq.add(new PrioritizedNode(newDist + heuristics(node, destination), node, n));
            }
        }
    }
}