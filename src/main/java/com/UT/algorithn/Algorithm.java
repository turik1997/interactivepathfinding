package com.UT.algorithn;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public abstract class Algorithm implements IAlgorithm {

    private int updateInterval = 50; // number of iterations after which UI redraws updates

    final protected Graph graph;
    final protected Node start;
    final protected Node destination;
    final protected BiConsumer<List<Node>,Integer> callback;
    final private LinkedList<Node> visited = new LinkedList<>();
    private int visitsFromLastUpdate = 0;
    private final int[] cameFrom;
    private final int[] usedNodes;


    public Algorithm(Graph graph, Node start, Node destination, BiConsumer<List<Node>,Integer> callback) {
        this.graph = graph;
        this.start = start;
        this.destination = destination;
        this.callback = callback;
        this.cameFrom = new int[this.graph.size()];
        this.usedNodes = new int[graph.size()/Integer.SIZE + 1];
        Arrays.fill(usedNodes, 0);
    }

    private Integer getId(Color color) {
        return ServerParams.valueToName.get(color.getRGB());
    }

    protected double cost(Color color) {
        Integer id = getId(color);
        if (id != null) return (double)ServerParams.cost[id];
        return 1.0;
    }

    protected boolean isAllowed(Color color) {
        Integer id = getId(color);
        if (id != null) return ServerParams.allowed[id];
        return true;
    }


    protected void visited(Node visitedNode) {
        visited.add(visitedNode);
        visitsFromLastUpdate++;
        if (visitsFromLastUpdate == updateInterval) {
            callback.accept(visited, 1);
            visitsFromLastUpdate = 0;
            visited.clear();
        }
    }

    protected List<Node> restorePath(Node start, Node end) {
        Node current = end;
        List<Node> path = new LinkedList<>();
        while (current.getId() != -1) {
            path.add(current);
            current = this.graph.getNode(cameFrom[current.getId()]);
        }

        Collections.reverse(path);
        return path;
    }

    protected void setParent(Node parent, Node node) {
        this.cameFrom[node.getId()] = parent.getId();
    }

    protected boolean isUsed(Node node) {
        int id = node.getId();
        int currentValue = usedNodes[getRoom(node)];
        return (currentValue & (1 << getBitPosition(node))) != 0;
    }

    protected void markUsed(Node node) {
        int id = node.getId();
        int room = getRoom(node);
        int valueInRoom = usedNodes[room];
        usedNodes[room] = usedNodes[room] | (1 << getBitPosition(node));
    }

    protected boolean hasReached(Node node) {
        return node == destination;
    }

    private int getBitPosition(Node node) {
        return node.getId() % Integer.SIZE;
    }

    private int getRoom(Node node) {
        return node.getId() / Integer.SIZE;
    }
}
