package com.UT.algorithn;

import com.UT.server.Updater;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class BFS extends Algorithm {

    private final Queue<Node> queue;

    public BFS(Graph graph, Node start, Node destination, Updater updater) {
        super(graph, start, destination, updater::update);
        this.queue = new LinkedList<>();
    }

    @Override
    public List<Node> solve() {
        Node current = start;
        setParent(graph.getNode(-1), current);
        queue.add(start);
        markUsed(current);
        while (!queue.isEmpty()) {
            current = queue.poll();
            visited(current);
            if (hasReached(current)) break;
            calculateNeighbors(current);
        }
        List<Node> path = restorePath(start, current);
        return path;
    }

    private void calculateNeighbors(Node node) {
        List<Node> neighbors = graph.getNeighbors(node);
        for ( Node n : neighbors ) {
            if (isUsed(n)) continue;
            Color color = new Color(n.r, n.g, n.b);
            if (!isAllowed(color)) continue;
            this.queue.add(n); markUsed(n);
            setParent(node, n);
        }
    }
}
