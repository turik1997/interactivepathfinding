package com.UT.algorithn;

import java.awt.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ServerParams {

    public final static int CANVAS_W = 640;
    public final static int CANVAS_H = 390;

    public final static class ColorName {
        public final static int WHITE = 0;
        public final static int BLUE = 1;
        public final static int GREEN = 2;
        public final static int RED = 3;
        public final static int BLACK = 4;
    }

    public static int cost[] = new int[] {1, 2, 4};
    public static boolean allowed[] = new boolean[5];
    public static final Map<Integer, Color> nameToValue;
    public static final Map<Integer, Integer> valueToName;
    static {
        Arrays.fill(allowed, true);
        allowed[ColorName.RED] = false;
        nameToValue = new HashMap<>();
        valueToName = new HashMap<>();
        nameToValue.put(ColorName.BLUE, new Color(30, 144, 255));
        nameToValue.put(ColorName.GREEN, new Color(255, 20, 147));
        nameToValue.put(ColorName.RED, new Color(220, 20, 60));
        valueToName.put(nameToValue.get(ColorName.BLUE).getRGB(), ColorName.BLUE);
        valueToName.put(nameToValue.get(ColorName.GREEN).getRGB(), ColorName.GREEN);
        valueToName.put(nameToValue.get(ColorName.RED).getRGB(), ColorName.RED);
    }

}
