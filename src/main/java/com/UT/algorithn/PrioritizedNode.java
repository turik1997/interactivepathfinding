package com.UT.algorithn;

public class PrioritizedNode {

    private final double priority;
    private final Node node;
    private final Node parent;

    public PrioritizedNode(double priority, Node parent, Node node) {
        this.priority = priority;
        this.node = node;
        this.parent = parent;
    }

    public Node getNode() {
        return node;
    }

    public double getPriority() {
        return priority;
    }

    public Node getParent() {
        return parent;
    }
}
