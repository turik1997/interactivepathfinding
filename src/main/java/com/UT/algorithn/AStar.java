package com.UT.algorithn;

import com.UT.server.Updater;

public class AStar extends Dijkstra {

    public AStar(Graph graph, Node start, Node destination, Updater updater) {
        super(graph, start, destination, updater::update);
    }

    @Override
    protected double heuristics(Node u, Node v) {
        return this.graph.dist(u, v);
    }
}
