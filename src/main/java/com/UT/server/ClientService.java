package com.UT.server;

import com.UT.algorithn.*;
import com.UT.server.messages.MessageBase64;
import com.UT.server.messages.VerticesListMessage;
import com.google.gson.Gson;
import org.eclipse.jetty.websocket.api.Session;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ClientService implements Callable<List<Node>> {

    private final Session session;
    private final Gson gson = new Gson();
    private final String txt;
    private final Class<?>[] classes = new Class[] {null, BFS.class, Dijkstra.class, AStar.class};
    private final Updater updater;

    public ClientService(Session session, String msg) {
        this.session = session;
        this.txt = msg;
        this.updater = new UpdaterImpl(session);
    }

    @Override
    public List<Node> call() throws Exception {
        System.out.println("Started path finding for " + session.getLocalAddress());
        System.out.println("The message is " + txt);
        MessageBase64 msg = gson.fromJson(txt, MessageBase64.class);
        GridGraph gridGraph = new GridGraph(msg.getBase64());
        System.out.println("Graph is ready");
        Algorithm algorithm = null;
        int algoType = msg.getAlgoType();
        if (algoType > 0 && algoType < 4) {
            System.out.println("We use " + classes[algoType]);
                algorithm = (Algorithm) classes[algoType].getConstructor(Graph.class, Node.class, Node.class, Updater.class).newInstance(new Object[] {gridGraph,
                        gridGraph.getNode(msg.getStartX(), msg.getStartY()),
                        gridGraph.getNode(msg.getFinishX(), msg.getFinishY()), this.updater});
        }

        List<Node> path = algorithm.solve();
        this.updater.update(path, 2);
        return path;
    }
}
