package com.UT.server;

import com.UT.algorithn.Node;
import com.UT.server.messages.VerticesListMessage;
import com.google.gson.Gson;
import org.eclipse.jetty.websocket.api.Session;

import java.io.IOException;
import java.util.List;

public class UpdaterImpl implements Updater {

    private final Session session;
    private final Gson gson = new Gson();

    public UpdaterImpl(Session session) {
        this.session = session;
    }

    @Override
    public void update(List<Node> path, int type) {
        try {
            this.session.getRemote().sendString(gson.toJson(new VerticesListMessage(type, path)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
