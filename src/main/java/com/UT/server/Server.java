package com.UT.server;

import com.UT.algorithn.Algorithm;
import com.UT.algorithn.BFS;
import com.UT.algorithn.ServerParams;
import com.UT.server.messages.ColorData;
import spark.ModelAndView;
import spark.Service;
import spark.Spark;
import spark.route.HttpMethod;
import spark.template.mustache.MustacheTemplateEngine;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Server {

    private Service http;
    private final ClientSocketHandler socketHandler = new ClientSocket();

    public void init() {
        this.http = initSparkServer();
        initWebSockets(http);
        initFilters(http);
        initRoutes(http);
    }

    private Service initSparkServer() {
        Service http = Service.ignite();
        http.ipAddress("0.0.0.0").port(4567);
        http.staticFiles.externalLocation("war/www/public");
        http.webSocketIdleTimeoutMillis(Integer.MAX_VALUE);

        return http;
    }


    private void initRoutes(Service http) {
        http.get("/", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            List<String> color = new ArrayList<>();
            String format = "rgb(%d, %d, %d)";
            Color blue = ServerParams.nameToValue.get(ServerParams.ColorName.BLUE);
            Color green = ServerParams.nameToValue.get(ServerParams.ColorName.GREEN);
            Color red = ServerParams.nameToValue.get(ServerParams.ColorName.RED);
            String bluergb = String.format(format, blue.getRed(), blue.getGreen(), blue.getBlue());
            String greenrgb = String.format(format, green.getRed(), green.getGreen(), green.getBlue());
            String redrgb = String.format(format, red.getRed(), red.getGreen(), red.getBlue());
            color.add(bluergb);
            color.add(greenrgb);
            color.add(redrgb);
            model.put("ids", color);
            List<ColorData> colorData = new ArrayList<>();
            String descFormat = "%dx cost";
            colorData.add(new ColorData(bluergb, String.format(descFormat, ServerParams.cost[ServerParams.ColorName.BLUE])));
            colorData.add(new ColorData(greenrgb, String.format(descFormat,  ServerParams.cost[ServerParams.ColorName.GREEN])));
            colorData.add(new ColorData(redrgb, "Prohibited area"));
            model.put("colors", colorData);

            return new MustacheTemplateEngine().render(new ModelAndView(model, "war/www/public/index.jsp"));
        });
    }

    private void initFilters(Service http) {
        // Remove trailing slashes
        http.before("*", (request, response) -> {
            if (request.pathInfo().endsWith("/") && request.pathInfo().length() > 1) {
                response.redirect(request.pathInfo().substring(0, request.pathInfo().length() - 1));
            }
        });
        // Filters here
    }

    private void initWebSockets(Service http) {
        http.webSocket("/shortestPathSocket", socketHandler);
    }
}
