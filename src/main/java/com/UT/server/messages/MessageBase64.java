package com.UT.server.messages;

public class MessageBase64 {

    private int algoType;

    private String base64;

    private int startX;
    private int startY;

    private int finishX;
    private int finishY;

    public int getAlgoType() {
        return algoType;
    }

    public String getBase64() {
        return this.base64;
    }

    public int getStartX() {
        return startX;
    }

    public int getStartY() {
        return startY;
    }

    public int getFinishX() {
        return finishX;
    }

    public int getFinishY() {
        return finishY;
    }
}
