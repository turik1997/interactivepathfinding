package com.UT.server.messages;

import com.UT.algorithn.Node;

import java.util.List;

public class VerticesListMessage {

    private final int type;
    private final List<Node> vertices;


    public VerticesListMessage(int type, List<Node> vertices) {
        this.type = type;
        this.vertices = vertices;
    }

    public int getType() {
        return type;
    }

    public List<Node> getVertices() {
        return vertices;
    }
}
