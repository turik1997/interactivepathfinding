package com.UT.server.messages;

public class ColorData {

    private String color;
    private String description;

    public ColorData(String color, String description) {
        this.color = color;
        this.description = description;
    }

    public String getColor() {
        return this.color;
    }

    public String getDescription() {
        return this.description;
    }
}
