package com.UT.server;

import com.UT.algorithn.GridGraph;
import com.UT.server.messages.MessageBase64;
import com.google.gson.Gson;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@WebSocket
public class ClientSocket implements ClientSocketHandler {

    private final Gson gson = new Gson();
    private final ExecutorService executorService = Executors.newFixedThreadPool(4);

    @Override
    @OnWebSocketConnect
    public void connected(Session session) {
    }

    @Override
    @OnWebSocketClose
    public void closed(Session session, int statusCode, String reason) {

    }

    @Override
    @OnWebSocketMessage
    public void message(Session session, byte[] bytes, int offset, int length) {

    }

    @Override
    @OnWebSocketMessage
    public void message(Session session, String text) {
        //MessageBase64 msg = gson.fromJson(text, MessageBase64.class);
        ClientService service = new ClientService(session, text);
        executorService.submit(service);
    }
}