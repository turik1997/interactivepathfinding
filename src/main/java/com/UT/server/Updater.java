package com.UT.server;

import com.UT.algorithn.Node;

import java.util.List;

public interface Updater {

    void update(List<Node> path, int type);
}
